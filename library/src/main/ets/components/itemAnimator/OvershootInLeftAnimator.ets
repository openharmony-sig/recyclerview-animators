/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import RecyclerView from '../RecyclerView'

@ComponentV2
export struct OvershootInLeftAnimator {
  @Param array: Array<string> = []
  @BuilderParam child: (itemData: ESObject) => void;
  @Param controller: RecyclerView.Controller = new RecyclerView.Controller()
  @Local private myArr: Array<RecyclerView.ArrayType> = []
  @Local private duration: number = 0
  private isClick: boolean = true
  @Local private trans: number = 0
  @Local x: string = '0%'
  @Local y: string = '0%'
  @Param columnsNum: number = 1
  private colNumStr: string = ''

  aboutToAppear() {
    this.controller.addValueCallback = (index: number, value: ESObject) => {
      this.addValue(index, value);
    }
    this.controller.deleteValueCallback = (index: number) => {
      this.deleteValue(index);
    }

    this.array.forEach((item: string) => {
      let newArr: RecyclerView.ArrayType = new RecyclerView.ArrayType(false, item);
      this.myArr.push(newArr)
    })
    for (let i = 0; i < this.columnsNum; i++) {
      this.colNumStr += '1fr '
    }
  }

  private addValue(index: number, value: ESObject) {
    if (!this.isClick) {
      return
    }
    this.isClick = false
    let newArr: RecyclerView.ArrayType = new RecyclerView.ArrayType(true, value);

    this.trans = 1
    this.duration = 0
    this.x = '-100%'
    this.y = '0%'
    this.myArr.splice(index, 0, newArr)

    setTimeout(() => {
      this.trans = 1
      this.x = '5%'
      this.y = '0%'
      this.duration = this.controller.duration
      this.myArr.splice(index, 1, newArr)

      setTimeout(() => {
        this.duration = 50
        this.x = '0%'
      }, this.controller.duration + 50)

      setTimeout(() => {
        this.myArr.forEach((item: RecyclerView.ArrayType) => {
          item.isNewValue = false
        })
        this.isClick = true
      }, this.controller.duration)

    }, 200)
  }

  deleteValue(index: number) {
    if (!this.isClick) {
      return
    }
    if (index >= this.myArr.length) {
      return
    }
    this.isClick = false

    let currentData: RecyclerView.ArrayType = this.myArr[index]
    currentData.isNewValue = true

    this.x = '-100%'
    this.y = '0%'
    this.trans = 1
    this.duration = this.controller.duration

    setTimeout(() => {
      this.duration = 0
      this.x = '0%'
      this.y = '0%'
      this.myArr.splice(index, 1, currentData)
      this.myArr.splice(index, 1)
      this.isClick = true
    }, this.controller.duration + 100)
  }

  build() {
    Grid() {
      ForEach(this.myArr, (item: RecyclerView.ArrayType) => {
        GridItem() {
          this.child(item.value)
        }
        .opacity(item.isNewValue ? this.trans : 1)
        .offset(item.isNewValue ? {
          x: this.x,
          y: this.y
        } : {})
        .animation({
          duration: this.duration, // 动画时长
          curve: Curve.Ease, // 动画曲线
          delay: 50, // 动画延迟
          iterations: 1, // 播放次数
          playMode: PlayMode.Normal // 动画模式
        })
      })
    }
    .columnsTemplate(this.colNumStr)
    .columnsGap(2)
    .rowsGap(2)
    .onScrollIndex((first: number) => {
      console.info(first.toString())
    })
    .width('100%')

  }
}