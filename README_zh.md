# recyclerview_animators

## 简介
> 带有添加删除动画效果以及整体动画效果的list组件库

![](animation.gif)

## 下载安装
```shell
ohpm install @ohos/recyclerview-animators
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装OpenHarmony ohpm 包 ](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明
1. 引入组件库

```
import { RecyclerView } from "@ohos/recyclerview-animators"
```

2. 在代码中使用
```
@State controller: RecyclerView.Controller = new RecyclerView.Controller()
private listDatas = ["A","B","C"]

aboutToAppear() {
    this.controller.setAdapterAnimation(RecyclerView.AdapterAnimationType.AlphaIn) // 设置列表整体效果类型
    this.controller.setFirstOnly(false) // 设置是否在item重复出现时显示动画效果
    this.controller.setDuration(500) // 设置动画时长
}

build() {
  Column() {
    RecyclerView({
      array: this.listDatas, // 数据源
      controller: this.controller, // 控制器
      child: (itemData) => {
        this.SpecificChild(itemData) // 子布局
      }
    })
  }
}

@Builder SpecificChild(itemData) {
    Column() {
      Image($r("app.media.chip"))
        .width('100%')
        .height(100)
      Text(itemData + '')
        .fontSize(20)
        .textAlign(TextAlign.Center)
        .width('100%')
    }.margin(10)
  }
```

## 接口说明
`controller: RecyclerView.Controller = new RecyclerView.Controller()`
1. 设置列表整体效果类型
   `this.controller.setAdapterAnimation()`
2. 设置是否在item重复出现时显示动画效果
   `this.controller.setFirstOnly()`
3. 设置动画时长
   `this.controller.setDuration()`

## 约束与限制

在下述版本验证通过：

- DevEco Studio NEXT Developer Beta3: 5.0(5.0.3.530)
- SDK: API12 (5.0.0.35(SP3))

## 目录结构
````
|---- recyclerview_animators  
|     |---- entry  # 示例代码文件夹
|     |---- library  # 库文件夹
|	    |----src
          |----main
              |----ets
                  |----components
                      |----adapterAnimator #动画效果适配
                      |----itemAnimator #元素动画效果实现
                      |----RecyclerView.ets #核心类
|           |---- Index.ets  # 对外接口
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/recyclerview-animators/issues) ，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/recyclerview-animators/pulls) 共建。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/recyclerview-animators/blob/master/LICENSE) ，请自由地享受和参与开源。
