## v2.1.0-rc.0
1. 适配新版状态管理装饰器

## v2.0.0

1. 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)
2. ArkTs新语法适配

## v1.1.1

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.1.0

1. 名称由recyclerview-animators-ets修改recyclerview-animators。
2. 旧的包@ohos/recyclerview-animators-ets已不维护，请使用新包@ohos/recyclerview-animators

## v1.0.2

- api8升级到api9
- 将工程转换为stage模型

## v1.0.0

- 已实现功能
1. 实现item动画效果：淡入淡出
2. 实现item动画效果：从上往下淡入
3. 实现item动画效果：从下往上淡入
4. 实现item动画效果：从左往右淡入
5. 实现item动画效果：从右往左淡入
6. 实现item动画效果：整体向内收缩
7. 实现item动画效果：从中间伸展
8. 实现item动画效果：从左侧伸展
9. 实现item动画效果：从右侧伸展
10. 实现item动画效果：从顶部伸展
11. 实现item动画效果：从底部伸展
12. 实现item动画效果：从左侧平移进入
13. 实现item动画效果：从右侧平移进入
14. 实现item动画效果：从下侧平移进入
15. 实现item动画效果：从上侧平移进入
16. 实现item动画效果：从左侧平移进入 弹性
17. 实现item动画效果：从右侧平移进入 弹性
18. 实现adapter动画效果：淡入
19. 实现adapter动画效果：从中间伸展
20. 实现adapter动画效果：从下侧平移进入
21. 实现adapter动画效果：从左侧平移进入
22. 实现adapter动画效果：从右侧平移进入

- 未实现功能
1. item动画：翻转
2. adapter动画的多列效果
