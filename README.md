# recyclerview_animators

## Introduction
> recyclerview_animators is a list component library that implements add and remove animations as well as overall animations.

![](animation.gif)

## How to Install
```shell
ohpm install @ohos/recyclerview-animators
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use
1. Import a component library.

```
import { RecyclerView } from "@ohos/recyclerview-animators"
```

2. Use it in code.
```
@State controller: RecyclerView.Controller = new RecyclerView.Controller()
private listDatas = ["A","B","C"]

aboutToAppear() {
    this.controller.setAdapterAnimation(RecyclerView.AdapterAnimationType.AlphaIn) // Set the overall list effect type.
    this.controller.setFirstOnly(false) // Set whether to display the animation when an item appears repeatedly.
    this.controller.setDuration(500) // Set the animation duration.
}

build() {
  Column() {
    RecyclerView({
      array: this.listDatas, // Data source
      controller: this.controller, // Controller
      child: (itemData) => {
        this.SpecificChild(itemData) // Child layout
      }
    })
  }
}

@Builder SpecificChild(itemData) {
    Column() {
      Image($r("app.media.chip"))
        .width('100%')
        .height(100)
      Text(itemData + '')
        .fontSize(20)
        .textAlign(TextAlign.Center)
        .width('100%')
    }.margin(10)
  }
```

## Available APIs
`controller: RecyclerView.Controller = new RecyclerView.Controller()`
1. Sets the overall list animation.
   `this.controller.setAdapterAnimation()`
2. Sets whether to display the animation when an item appears repeatedly.
   `this.controller.setFirstOnly()`
3. Sets the animation duration.
   `this.controller.setDuration()`

## Constraints

This project has been verified in the following versions:

- DevEco Studio NEXT Developer Beta3: 5.0 (5.0.3.530)
- SDK: API 12 (5.0.0.35 (SP3))

## Directory Structure
````
|---- recyclerview_animators  
|     |---- entry  # Sample code
|     |---- library  # Library
|	    |----src
          |----main
              |----ets
                  |----components
                      |----adapterAnimator # Animation adaptation
                      |----itemAnimator # Item animation implementation
                      |----RecyclerView.ets # Core class
|           |---- Index.ets  # External APIs
|     |---- README.md  # Readme                   
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/recyclerview-animators/issues) or a [PR](https://gitee.com/openharmony-sig/recyclerview-animators/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/recyclerview-animators/blob/master/LICENSE).
